# README #

This is a Java library for the ESS electronic logbook (https://logbook.esss.lu.se). It has a limited functionality compared to the oficial web interface and its main purpose is to allow the submission of new entries from Java applications.

This library started as a feature for Open XAL to be able to post directly from applications, including files and screenshots as attachments.

More info can be found at https://confluence.esss.lu.se/display/SW/ESS+Electronic+Logbook .

### Features ###

* Class to submit new entries with attachments.
* JavaFX interface.
* Credentials handling that allows to remember a logged user.

### Developers ###

* Juan F. Esteban Müller
