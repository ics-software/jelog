/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.ess.jelog;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author juanfestebanmuller
 */
public class Action {

    enum TYPE {
        OR,
        AND,
        TRIGGER
    }

    private TYPE type = TYPE.TRIGGER;
    private List<Action> children = new ArrayList<>();
    private String trigger = null;
    private List<String> action;

    public Action(String trigger, List<String> action) {
        this.action = action;
        if (trigger.contains(",")) {
            String[] triggers = trigger.split(",");
            this.type = TYPE.OR;
            for (String trigger_i : triggers) {
                children.add(new Action(trigger_i));
            }
        } else if (trigger.contains("&")) {
            String[] triggers = trigger.split("&");
            this.type = TYPE.AND;
            for (String trigger_i : triggers) {
                children.add(new Action(trigger_i));
            }
        } else {
            this.trigger = trigger;
        }
    }

    private Action(String trigger) {
        this(trigger, null);
    }

    public List<String> getAction() {
        return action;
    }

    public boolean evaluate(Collection<String> triggers) {
        if (!triggers.isEmpty()) {
            switch (type) {
                case TRIGGER:
                    for (String t : triggers) {
                        if (trigger.equals(t)) {
                            return true;
                        }
                    }
                    return false;
                case OR:
                    for (Action action : children) {
                        if (action.evaluate(triggers)) {
                            return true;
                        }
                    }
                    return false;
                case AND:
                    for (Action action : children) {
                        if (!action.evaluate(triggers)) {
                            return false;
                        }
                    }
                    return true;
                default:
                    return false;
            }
        } else {
            return false;
        }
    }
}
