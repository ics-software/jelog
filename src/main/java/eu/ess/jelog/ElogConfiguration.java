/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package eu.ess.jelog;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class downloads and parses the elog configuration as a nested list.
 *
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 */
public class ElogConfiguration extends LogbookNode {

    private URL server;

    private final static String REPLY_TO_ATT = "Reply to";
    private final boolean parsed;

    public boolean isParsed() {
        return parsed;
    }

    /**
     * Creates a new ElogConfiguration object for a given server address and
     * parses the configuration.
     *
     * @param server
     */
    public ElogConfiguration(URL server) {
        this.server = server;

        parsed = parse();
    }

    @Override
    public boolean isParent() {
        return true;
    }

    /**
     * This method parses the configuration. So far, it only read logbooks and
     * logbook groups, and their attributes.
     */
    private boolean parse() {
        setGroup(true);

        LogbookNode currentNode = this;

        InputStream file = null;
        try {
            file = new URL(server, "?cmd=GetConfig").openStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(file));
            String line;

            while ((line = reader.readLine()) != null) {
                // Create logbook structure
                if (line.startsWith("Top group") || line.startsWith("Group")) {
                    String groupName;
                    String[] subNodes;
                    if (line.startsWith("Top group")) {
                        groupName = line.substring("Top group ".length(), line.indexOf("=")).trim();
                        subNodes = line.substring(line.indexOf("=") + 2).trim().split(",[\\s]*");
                    } else {
                        groupName = line.substring("Group ".length(), line.indexOf("=")).trim();
                        subNodes = line.substring(line.indexOf("=") + 2).trim().split(",[\\s]*");
                    }

                    LogbookNode groupNode = this.pop(groupName);
                    if (groupNode == null) {
                        groupNode = new LogbookNode(groupName);
                        this.addNode(groupName, groupNode);
                    }
                    groupNode.setGroup(true);

                    for (String nodeName : subNodes) {
                        LogbookNode subNode = pop(nodeName);
                        if (subNode == null) {
                            subNode = new LogbookNode(nodeName);
                        }
                        groupNode.addNode(nodeName, subNode);
                    }
                    // Parsing attributes
                } else if (line.startsWith("[")) {
                    if (line.startsWith("[global]")) {
                        currentNode = this;
                    } else if (line.startsWith("[global ")) {
                        currentNode = getLogbookNode(line.substring("[global ".length(), line.indexOf(']')));
                    } else {
                        currentNode = getLogbookNode(line.substring(1, line.indexOf(']')));
                    }
                } else if (line.contains("Attributes") && (line.startsWith("Attributes") || line.split(" ")[1].equals("Attributes"))) {
                    // If no attributes created yet
                    if (currentNode.attributes == null) {
                        currentNode.setAttributes(new LinkedHashMap<>(currentNode.getAttributes()));
                    }

                    String[] attributeList = line.substring(line.indexOf("=") + 2).trim().split(",[\\s]*");

                    boolean setRequired = false;
                    boolean setLocked = false;
                    if (line.startsWith("Required")) {
                        setRequired = true;
                    } else if (line.startsWith("Locked")) {
                        setLocked = true;
                    } else {
                        // Keep only the attributes given by this option (hard copy).
                        LinkedHashMap<String, LogbookAttribute> newAttributes = new LinkedHashMap<>();
                        for (String attributeName : currentNode.getAttributes().keySet()) {
                            for (String includedAttribute : attributeList) {
                                if (attributeName.equals(includedAttribute) || attributeName.equals(REPLY_TO_ATT)) {
                                    newAttributes.put(attributeName, new LogbookAttribute(currentNode.getAttributes().get(attributeName)));
                                }
                            }
                        } 
                       currentNode.setAttributes(newAttributes);
                    }

                    for (String attributeName : attributeList) {
                        LogbookAttribute attribute;
                        if (!currentNode.getAttributes().containsKey(attributeName)) {
                            attribute = new LogbookAttribute();
                            currentNode.getAttributes().put(attributeName, attribute);
                        } else {
                            attribute = currentNode.getAttributes().get(attributeName);
                        }
                        attribute.setLocked(setLocked);
                        attribute.setRequired(setRequired);
                    }
                } else if (line.startsWith("Options") || line.startsWith("MOptions") || line.startsWith("ROptions")) {
                    // If no attributes created yet
                    if (currentNode.attributes == null) {
                        currentNode.setAttributes(new LinkedHashMap<>(currentNode.getAttributes()));
                    }

                    boolean setMultiOptions = false;
                    boolean setRadioOptions = false;
                    if (line.startsWith("MOptions")) {
                        setMultiOptions = true;
                    } else if (line.startsWith("ROptions")) {
                        setRadioOptions = true;
                    }
                    String attributeName = line.substring(line.indexOf("Options") + "Options ".length(), line.indexOf("=")).strip();

                    LogbookAttribute attribute;
                    if (!currentNode.getAttributes().containsKey(attributeName)) {
                        attribute = new LogbookAttribute();
                        currentNode.getAttributes().put(attributeName, attribute);
                    } else {
                        attribute = currentNode.getAttributes().get(attributeName);
                    }
                    attribute.setMultioption(setMultiOptions);
                    attribute.setRadiooption(setRadioOptions);

                    String[] options = line.substring(line.indexOf("=") + 2).trim().replaceAll("\\{(\\w+)\\}", "").split(",[\\s]*");
                    attribute.setOptions(options);

                    String[] triggers = line.substring(line.indexOf("=") + 2).trim().split(",[\\s]*");
                    for (int i = 0; i < triggers.length; i++) {
                        if (triggers[i].contains("{")) {
                            triggers[i] = triggers[i].substring(triggers[i].indexOf("{") + 1, triggers[i].indexOf("}"));
                        } else {
                            triggers[i] = "";
                        }
                    }

                    attribute.setTriggers(triggers);
                } else if (line.startsWith("Menu commands") && line.contains("Reply")) {
                    // If no attributes created yet
                    if (currentNode.attributes == null) {
                        currentNode.setAttributes(new LinkedHashMap<>(currentNode.getAttributes()));
                    }

                    // Adding the reply to option first.
                    if (!currentNode.getAttributes().containsKey(REPLY_TO_ATT)) {
                        LogbookAttribute attribute = new LogbookAttribute();
                        currentNode.getAttributes().put(REPLY_TO_ATT, attribute);
                    }
                } else if (line.startsWith("Preset")) {
                    String attributeName = null;
                    if (line.startsWith("Preset on")) {
                        if (line.startsWith("Preset on edit")) {
                            attributeName = line.substring(line.indexOf("Preset on edit") + "Preset on edit ".length(), line.indexOf("=")).strip();
                        } else {
                            continue;
                        }
                    } else {
                        attributeName = line.substring(line.indexOf("Preset") + "Preset ".length(), line.indexOf("=")).strip();
                    }
                    LogbookAttribute attribute;
                    if (!currentNode.getAttributes().containsKey(attributeName)) {
                        attribute = new LogbookAttribute();
                        currentNode.getAttributes().put(attributeName, attribute);
                    } else {
                        attribute = currentNode.getAttributes().get(attributeName);
                    }
                    attribute.setPreset(true);
                } else if (line.matches("\\{(.)+\\} Preset (.)+")) {
                    String trigger = line.substring(line.indexOf("{") + 1, line.indexOf("}")).strip();
                    String attributeName = line.substring(line.indexOf("Preset") + "Preset ".length(), line.indexOf("=")).strip();
                    String actions = line.substring(line.indexOf("=") + 2).trim();

                    LogbookAttribute attribute;
                    if (!currentNode.getAttributes().containsKey(attributeName)) {
                        attribute = new LogbookAttribute();
                        currentNode.getAttributes().put(attributeName, attribute);
                    } else {
                        attribute = currentNode.getAttributes().get(attributeName);
                    }
                    attribute.addActions(new Action(trigger, Arrays.asList(actions)));
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(ElogConfiguration.class.getName()).log(Level.SEVERE, "Couldn''t connect to {0}", server);
            return false;
        } finally {
            try {
                if (file != null) {
                    file.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(ElogConfiguration.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return true;
    }
}
