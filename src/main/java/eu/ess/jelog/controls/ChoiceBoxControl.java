/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package eu.ess.jelog.controls;

import eu.ess.jelog.TriggerListener;
import java.util.Arrays;
import java.util.List;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.ChoiceBox;

/**
 *
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 */
public class ChoiceBoxControl extends DecoratedHBox implements Control {

    private ChoiceBox<String> choiceBox;

    public ChoiceBoxControl(String[] items) {
        super("attribvalue");
        choiceBox = new ChoiceBox();
        choiceBox.getItems().addAll(Arrays.asList(items));
        choiceBox.getSelectionModel().selectFirst();
        this.add(choiceBox);
    }

    @Override
    public List<String> getOptions() {
        return Arrays.asList(choiceBox.getSelectionModel().getSelectedItem());
    }

    @Override
    public void setDisabled() {
        choiceBox.setDisable(true);
    }

    @Override
    public void setOptions(List<String> options) {
        if (!options.isEmpty()) {
            choiceBox.getSelectionModel().select(options.get(0));
        }
    }

    @Override
    public void registerTriggers(String attributeName, String[] triggers, TriggerListener listener) {
        choiceBox.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number oldNumber, Number newNumber) {
                listener.trigger(attributeName, triggers[(Integer) newNumber]);
            }
        });
        // Trigger for the first time.
        listener.trigger(attributeName, triggers[choiceBox.getSelectionModel().getSelectedIndex()]);
    }
}
