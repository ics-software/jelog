/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package eu.ess.jelog.controls;

import eu.ess.jelog.TriggerListener;
import java.util.ArrayList;
import java.util.List;
import javafx.geometry.Pos;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.FlowPane;

/**
 *
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 */
public class CheckBoxControl extends FlowPane implements Control {

    private List<CheckBox> checkBoxList = new ArrayList<>();

    public CheckBoxControl(String[] items) {
        super();
        setAlignment(Pos.CENTER_LEFT);
        setHgap(15);
        getStyleClass().add("attribvalue");

        for (String item : items) {
            CheckBox checkBox = new CheckBox(item);
            checkBoxList.add(checkBox);
        }
        getChildren().addAll(checkBoxList);

        // Sure?
        if (checkBoxList.size() == 1) {
            checkBoxList.get(0).setSelected(true);
        }
    }

    @Override
    public List<String> getOptions() {
        List<String> checkedItems = new ArrayList<>();

        for (CheckBox checkBox : checkBoxList) {
            if (checkBox.isSelected()) {
                checkedItems.add(checkBox.getText());
            }
        }

        return checkedItems;
    }

    @Override
    public void setDisabled() {
        for (CheckBox checkBox : checkBoxList) {
            checkBox.setDisable(true);
        }
    }

    @Override
    public void setOptions(List<String> options) {
        for (String checkedOption : options) {
            for (CheckBox checkBox : checkBoxList) {
                if (checkBox.getText().equals(checkedOption.strip())) {
                    checkBox.setSelected(true);
                }
            }
        }
    }

    @Override
    public void registerTriggers(String attributeName, String[] triggers, TriggerListener listener) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
