/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package eu.ess.jelog.controls;

import eu.ess.jelog.TriggerListener;
import java.util.Arrays;
import java.util.List;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;

/**
 *
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 */
public class TextFieldControl extends DecoratedHBox implements Control {

    private TextField textField;

    public TextFieldControl() {
        super("attribvalue");
        textField = new TextField();
        HBox.setHgrow(textField, Priority.ALWAYS);
        this.add(textField);
    }

    @Override
    public List<String> getOptions() {
        return Arrays.asList(textField.getText());
    }

    @Override
    public void setDisabled() {
        textField.setDisable(true);
    }

    @Override
    public void setOptions(List<String> options) {
        if (!options.isEmpty()) {
            textField.setText(options.get(0));
        }
    }

    @Override
    public void registerTriggers(String attributeName, String[] triggers, TriggerListener listener) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
