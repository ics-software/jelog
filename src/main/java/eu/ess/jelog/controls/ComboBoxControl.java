/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package eu.ess.jelog.controls;

import eu.ess.jelog.TriggerListener;
import java.util.Arrays;
import java.util.List;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.ComboBox;

/**
 *
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 */
public class ComboBoxControl extends DecoratedHBox implements Control {

    private ComboBox<String> comboBox;

    public ComboBoxControl(String[] items) {
        super("attribvalue");
        comboBox = new ComboBox();
        comboBox.getItems().addAll(Arrays.asList(items));
        comboBox.getSelectionModel().selectFirst();
        this.add(comboBox);
    }

    @Override
    public List<String> getOptions() {
        return Arrays.asList(comboBox.getSelectionModel().getSelectedItem());
    }

    @Override
    public void setDisabled() {
        comboBox.setDisable(true);
    }

    @Override
    public void setOptions(List<String> options) {
        if (!options.isEmpty()) {
            comboBox.getSelectionModel().select(options.get(0));
        }
    }

    @Override
    public void registerTriggers(String attributeName, String[] triggers, TriggerListener listener) {
        comboBox.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number oldNumber, Number newNumber) {
                listener.trigger(attributeName, triggers[(Integer) newNumber]);
            }
        });
    }
}
