/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.ess.jelog;

import java.net.URL;
import java.util.TreeMap;
import java.util.Map;

/**
 *
 * @author juanfestebanmuller
 */
public class LogbookNode {

    protected LogbookNode parent = null;
    protected boolean group;
    protected String name;
    protected Map<String, LogbookNode> nodes = new TreeMap<>();
    protected Map<String, LogbookAttribute> attributes;

    public LogbookNode() {
    }

    public LogbookNode(URL url) {
    }

    public LogbookNode(String name) {
        this.name = name;
    }

    public LogbookNode getParent() {
        return parent;
    }

    public void setParent(LogbookNode parent) {
        this.parent = parent;
    }

    public boolean isGroup() {
        return group;
    }

    public void setGroup(boolean group) {
        this.group = group;
    }

    public String getName() {
        return name;
    }

    public boolean isParent() {
        return false;
    }

    /**
     * Remove the LogbookNode "name" from the list and returns it.
     *
     * @param name Name of the LogbookNode element searched for.
     * @return The LogbookNode or null if it doesn't exist.
     */
    public LogbookNode pop(String name) {
        for (String nodeName : nodes.keySet()) {
            LogbookNode node = nodes.get(nodeName);
            if (nodeName.equals(name)) {
                nodes.remove(nodeName);
                node.setParent(null);
                return node;
            }
            if (node.isGroup()) {
                node = node.pop(name);
                if (node != null) {
                    return node;
                }
            }
        }

        return null;
    }

    /**
     * Returns the LogbookNode "name" from the list without removing it.
     *
     * @param name Name of the LogbookNode element searched for.
     * @return The LogbookNode or null if it doesn't exist.
     */
    public LogbookNode getLogbookNode(String name) {
        for (String nodeName : nodes.keySet()) {
            LogbookNode node = nodes.get(nodeName);
            if (nodeName.equals(name)) {
                return node;
            }
            if (node.isGroup()) {
                LogbookNode node2 = node.getLogbookNode(name);
                if (node2 != null) {
                    return node2;
                }
            }
        }

        return null;
    }

    public Map<String, LogbookNode> getNodes() {
        return nodes;
    }

    public void addNode(String name, LogbookNode node) {
        node.setParent(this);
        nodes.put(name, node);
    }

    public LogbookAttribute getAttribute(String attributeName) {
        return attributes.get(attributeName);
    }

    public void setAttributes(Map<String, LogbookAttribute> attributes) {
        this.attributes = attributes;
    }

    public Map<String, LogbookAttribute> getAttributes() {
        if (attributes != null) {
            return attributes;
        } else if (parent != null) {
            return parent.getAttributes();
        } else {
            attributes = new TreeMap<>();
            return attributes;
        }
    }

    public void addAttribute(String name, LogbookAttribute attribute) {
        attributes.put(name, attribute);
    }

    public Map<String, LogbookNode> getGroups() {
        Map<String, LogbookNode> groups = new TreeMap<>();

        nodes.values().stream().filter((node) -> (node.isGroup())).forEachOrdered((node) -> {
            groups.put(node.getName(), node);
        });
        return groups;
    }

    public Map<String, LogbookNode> getLogbooks() {
        Map<String, LogbookNode> logbooks = new TreeMap<>();

        nodes.values().stream().filter((node) -> (!node.isGroup())).forEachOrdered((node) -> {
            logbooks.put(node.getName(), node);
        });
        return logbooks;
    }

    /**
     * Method to get attributes for a particular logbook. If the name is null,
     * it gives global attributes.
     *
     * @param logbookName
     * @return
     */
    public Map<String, LogbookAttribute> getAttributes(String logbookName) {
        if (logbookName == null) {
            return getAttributes();
        } else {
            return getLogbookNode(logbookName).getAttributes();
        }
    }
}
