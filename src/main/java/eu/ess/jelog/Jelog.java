/*
 * Copyright (C) 2018 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package eu.ess.jelog;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 * Library to post new entries to elog. For the moment, it only implements
 * posting new entries or replies, with images or other attachments. In the
 * future it may implement browsing the logbook, and edit/delete existing
 * entries.
 *
 * @author Juan F. Esteban Müller <juanf.estebanmuller@esss.se>
 */
public class Jelog {

    private static final String ESS_ELOG = "https://logbook.esss.lu.se";

    private final URL server;
    private ElogConfiguration configuration;

    private final static char[] MULTIPART_CHARS
            = "_1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
                    .toCharArray();

    private String userName;
    private String userFullName;
    private String userPasswordHash;

    /**
     * Method to generate a random Boundary String for the HTTP message.
     *
     * @return Boundary String
     */
    protected static String generateBoundary() {
        StringBuilder buffer = new StringBuilder();
        Random rand = new Random();
        int count = rand.nextInt(11) + 30; // a random size from 30 to 40
        for (int i = 0; i < count; i++) {
            buffer.append(MULTIPART_CHARS[rand.nextInt(MULTIPART_CHARS.length)]);
        }
        return buffer.toString();
    }

    /**
     * Instantiates a new Jelog class for a given server.It connects to the
     * server address and downloads the configuration.
     *
     * @param elogAddress Server URL
     * @throws java.net.MalformedURLException
     */
    public Jelog(String elogAddress) throws MalformedURLException {
        server = new URL(elogAddress);

        setTrustAllCerts();

        configuration = new ElogConfiguration(server);

        retrieveUsernameAndPassword();
    }

    /**
     * Instantiates a new Jelog class for the default server. It connects to the
     * server address and downloads the configuration.
     *
     * @throws java.net.MalformedURLException
     */
    public Jelog() throws MalformedURLException {
        this(ESS_ELOG);
    }

    /**
     * To try to connect again to the logbook and get the configuration data.
     */
    public void retry() {
        configuration = new ElogConfiguration(server);
    }

    public boolean isConnected() {
        return configuration.isParsed();
    }

    public String getServer() {
        return server.toExternalForm();
    }

    public URL getServerURL() {
        return server;
    }

    public String getUserName() {
        return userName;
    }

    public String getUserFullName() {
        return userFullName;
    }

    public String getUserPasswordHash() {
        return userPasswordHash;
    }

    public LogbookNode getConfiguration() {
        return configuration;
    }

    public Map<String, LogbookNode> getLogbooks() {
        return configuration.getLogbooks();
    }

    public Map<String, LogbookNode> getGroups() {
        return configuration.getGroups();
    }

    public Map<String, LogbookAttribute> getLogbookAttributes(String logbook) {
        return configuration.getLogbookNode(logbook).getAttributes();
    }

    public static void setTrustAllCerts() {
        TrustManager[] trustAllCerts = new TrustManager[]{
            new X509TrustManager() {
                @Override
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                @Override
                public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                }

                @Override
                public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                }
            }
        };

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(
                    new HostnameVerifier() {
                @Override
                public boolean verify(String urlHostName, SSLSession session) {
                    return true;
                }
            });
        } catch (KeyManagementException | NoSuchAlgorithmException ex) {
            Logger.getLogger(Jelog.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Method to log in.It will generate a cookie that can be used later for
     * posting new entries.
     *
     * @param username User name.
     * @param password In clear text, no hash.
     * @param remember Set to true to remember login for 31 days.
     * @return true if login successful.
     */
    public boolean login(String username, char[] password, boolean remember) {
        try {
            // Setting the default cookie manager
            CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));

            // Connect to the web server endpoint
            HttpURLConnection urlConnection = (HttpURLConnection) server.openConnection();

            String boundaryString = "-----" + Jelog.generateBoundary();

            // Indicate that we want to write to the HTTP request body
            urlConnection.setDoOutput(true);
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundaryString);

            OutputStream outputStreamToRequestBody = urlConnection.getOutputStream();
            BufferedWriter httpsRequestBodyWriter
                    = new BufferedWriter(new OutputStreamWriter(outputStreamToRequestBody, "ISO-8859-1"));

            // Specify redir parameter
            sendPartPost(httpsRequestBodyWriter, "redir", "", boundaryString);

            // Specify user name
            sendPartPost(httpsRequestBodyWriter, "uname", username, boundaryString);

            // Specify password
            sendPasswordPost(httpsRequestBodyWriter, "upassword", password, boundaryString);

            // Specify remember
            if (remember) {
                sendPartPost(httpsRequestBodyWriter, "remember", "1", boundaryString);
            }

            // Mark the end of the multipart http request
            httpsRequestBodyWriter.write("\r\n--" + boundaryString + "--\r\n");
            httpsRequestBodyWriter.flush();

            // Close the streams
            outputStreamToRequestBody.close();
            httpsRequestBodyWriter.close();

            Integer responseCode = urlConnection.getResponseCode();
            if (responseCode != HttpURLConnection.HTTP_OK) {
                String msg = "Error " + responseCode.toString()
                        + " received when trying to submit the new entry.";
                Logger.getLogger(Jelog.class.getName()).log(Level.INFO, msg);
            }

            BufferedReader login_response = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String line;
            while ((line = login_response.readLine()) != null) {
                if (line.contains("dlgerror")) {
                    String msg = "Invalid user name or password!";
                    Logger.getLogger(Jelog.class.getName()).log(Level.INFO, msg);
                    login_response.close();
                    return false;
                }
            }

            login_response.close();
        } catch (IOException ex) {
            Logger.getLogger(Jelog.class.getName()).log(Level.SEVERE, "Couldn't log in to the logbook.");
            return false;
        }

        return true;
    }

    public boolean login(String username, char[] password) throws IOException {
        return login(username, password, false);
    }

    public void logout() {
        CookieManager manager = (CookieManager) CookieHandler.getDefault();
        manager.getCookieStore().removeAll();
    }

    /**
     * If the right cookie is available, this method returns the user full name,
     * username and password hash (SHA-256).
     */
    final void retrieveUsernameAndPassword() {
        String author = null;
        String username = null;
        String passwordHash = null;

        if (configuration.isParsed()) {
            LogbookNode logbookNode = configuration;
            while (logbookNode.isGroup()) {
                if (logbookNode.getLogbooks().isEmpty()) {
                    logbookNode = (LogbookNode) ((TreeMap) logbookNode.getGroups()).firstEntry().getValue();

                } else {
                    logbookNode = (LogbookNode) ((TreeMap) logbookNode.getLogbooks()).firstEntry().getValue();
                }
            }

            String logbookUrl = (server.toString().endsWith("/") ? server.toString() : server.toString() + '/');
            String elog = logbookUrl + logbookNode.getName() + "/?cmd=New";

            Document doc;
            try {
                doc = Jsoup.connect(elog).get();
                for (Element element : doc.getElementsByAttribute("name")) {
                    switch (element.attr("name")) {
                        case "Author":
                            author = element.attr("value");
                            break;
                        case "unm":
                            username = element.attr("value");
                            break;
                        case "upwd":
                            passwordHash = element.attr("value");
                            break;
                        default:
                            break;
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(Jelog.class.getName()).log(Level.SEVERE, null, ex);
            }

            this.userName = username;
            this.userFullName = author;
            this.userPasswordHash = passwordHash;
        }
    }

    /**
     * Method to submit a new entry to a logbook.
     *
     * @param fields The additional fields of the entry.
     * @param body Text body of the message. Can be plain text, HTML, or ELCode.
     * @param encoding Encoding, can be plain, HTML, or ELCode.
     * @param attachments Optional attachments.
     * @param logbook Name of the logbook to be used to post.
     *
     * @return Http respose code. If everything was ok, it should return 200.
     * @throws IOException if the URL is not valid
     */
    public int submit(Map<String, List<String>> fields, String body,
            String encoding, List<Attachment> attachments, String logbook) throws IOException {

        retrieveUsernameAndPassword();

        URL elog = new URL(server, logbook);

        // Connect to the web server endpoint
        HttpURLConnection urlConnection = (HttpURLConnection) elog.openConnection();

        String boundaryString = "-----" + generateBoundary();

        // Indicate that we want to write to the HTTP request body
        urlConnection.setDoOutput(true);
        urlConnection.setRequestMethod("POST");
        urlConnection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundaryString);

        OutputStream outputStreamToRequestBody = urlConnection.getOutputStream();
        BufferedWriter httpsRequestBodyWriter
                = new BufferedWriter(new OutputStreamWriter(outputStreamToRequestBody, "ISO-8859-1"));

        // Specify command (Submit)
        sendPartPost(httpsRequestBodyWriter, "cmd", "Submit", boundaryString);

//        // Suppress email notifications (no STMP server defined returns 404)
//        sendPartPost(httpsRequestBodyWriter, "suppress", "1", boundaryString);
        if (userName != null && userPasswordHash != null) {
            // Specify user name
            sendPartPost(httpsRequestBodyWriter, "unm", userName, boundaryString);
            // Specify password
            sendPartPost(httpsRequestBodyWriter, "upwd", userPasswordHash, boundaryString);
        }

        // If the message is a reply, specify it
        if (fields.get("Reply to") != null && !fields.get("Reply to").isEmpty() && !"".equals(fields.get("Reply to").get(0))) {
            sendPartPost(httpsRequestBodyWriter, "reply_to", fields.get("Reply to").get(0), boundaryString);
        }

        // Creates a new field for the author or overwrite the existing one with
        // the user's name returned from elog server if the field is locked.
        if (userFullName != null && (configuration.getLogbookNode(logbook).getAttribute("Author").isLocked() || !fields.containsKey("Author"))) {
            fields.put("Author", Arrays.asList(userFullName));
        }

        // Send all other fields
        for (Entry<String, List<String>> field : fields.entrySet()) {
            String attributes = "";
            if (field.getValue().size() == 1) {
                attributes = field.getValue().get(0);
            } else {
                for (String item : field.getValue()) {
                    attributes = attributes.concat(item + " | ");
                }

                if (!attributes.isEmpty()) {
                    attributes = attributes.substring(0, attributes.length() - 3);
                }
            }
            sendPartPost(httpsRequestBodyWriter, field.getKey().replace(" ", "_"), attributes.trim(), boundaryString);
        }

        // Set default encoding
        if (encoding == null) {
            encoding = "HTML";
        }

        if (encoding.equals("plain") || encoding.equals("HTML") || encoding.equals("ELCode")) {
            // Specify encoding
            sendPartPost(httpsRequestBodyWriter, "encoding", encoding, boundaryString);
        } else {
            throw new RuntimeException("Invalid message encoding. Valid options: plain, HTML, ELCode.");
        }

        // Specify text
        sendPartPost(httpsRequestBodyWriter, "Text", body, boundaryString);

        // Write other attachments (if available)
        if (attachments != null) {
            for (Attachment attachment : attachments) {
                if (attachment != null) {
                    httpsRequestBodyWriter.write("--" + boundaryString + "\r\n");
                    httpsRequestBodyWriter.write("Content-Disposition: form-data;"
                            + "name=\"attfile\";"
                            + "filename=\"" + attachment.getFileName() + "\""
                            + "\r\nContent-Type: " + attachment.getMimeType() + "\r\n");
                    httpsRequestBodyWriter.write("Content-Transfer-Encoding: binary\r\n\r\n");
                    httpsRequestBodyWriter.flush();

                    attachment.writeTo(outputStreamToRequestBody);
                }
            }
        }

        // Mark the end of the multipart http request
        httpsRequestBodyWriter.write("\r\n--" + boundaryString + "--\r\n");
        httpsRequestBodyWriter.flush();

        // Close the streams
        outputStreamToRequestBody.close();
        httpsRequestBodyWriter.close();

        Integer responseCode = urlConnection.getResponseCode();
        if (responseCode != HttpURLConnection.HTTP_OK) {
            Logger.getLogger(Jelog.class.getName()).log(Level.WARNING, "Response message:\n{0}", urlConnection.getResponseMessage());
            throw new RuntimeException("Error " + responseCode.toString()
                    + " received when trying to submit the new entry.");
        }

        // Get message ID of the new entry
        urlConnection.getInputStream().close();

        String entryURL = urlConnection.getURL().getFile();

        // Strip any error message
        int indexQM = entryURL.lastIndexOf('?');
        if (indexQM != -1) {
            entryURL = entryURL.substring(0, indexQM);
        }

        int indexId = entryURL.lastIndexOf('/') + 1;

        return Integer.parseInt(entryURL.substring(indexId));
    }

    /**
     * Method to submit a part of a multi-part post http request
     */
    private void sendPartPost(BufferedWriter writer, String name, String value, String boundaryString) throws IOException {
        writer.write("--" + boundaryString + "\r\n");
        writer.write("Content-Disposition: form-data; name=\"" + name + "\"");
        writer.write("\r\n\r\n");
        writer.write(value);
        writer.write("\r\n");
        writer.flush();
    }

    private void sendPasswordPost(BufferedWriter writer, String name, char[] value, String boundaryString) throws IOException {
        writer.write("--" + boundaryString + "\r\n");
        writer.write("Content-Disposition: form-data; name=\"" + name + "\"");
        writer.write("\r\n\r\n");
        for (char c : value) {
            writer.write(c);
        }
        writer.write("\r\n");
        writer.flush();
    }
}
