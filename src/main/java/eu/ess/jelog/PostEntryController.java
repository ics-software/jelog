/*
 * Copyright (C) 2018 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package eu.ess.jelog;

import eu.ess.jelog.controls.AttachmentPane;
import eu.ess.jelog.controls.CheckBoxControl;
import eu.ess.jelog.controls.ChoiceBoxControl;
import eu.ess.jelog.controls.Control;
import eu.ess.jelog.controls.DecoratedHBox;
import eu.ess.jelog.controls.DecoratedLabel;
import eu.ess.jelog.controls.TextFieldControl;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.LinkedHashMap;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Dialog;
import javafx.scene.web.WebView;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.web.HTMLEditor;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.util.Pair;

/**
 * FXML Controller class for PostEntryScene.fxml.
 *
 * For the editor, it tries to get CKEditor on a WebView by looking for
 * editor.html in the property eu.ess.jelog.ckeditor_path or, if missing, in the
 * current working directory. If not found, it uses the JavaFX HTMLEditor.
 *
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 */
public class PostEntryController implements Initializable, TriggerListener {

    private WebView webView;
    private HTMLEditor htmlEditor;

    private static String elogServer = "https://logbook.esss.lu.se/";

    private static final String TEXT_ATTRIBUTE_NAME = "Text";

    private static final String TEMPLATES_URL = "790101_000000";

    private static final String OPENING_HTML_TAG = "<html dir=\"ltr\"><head></head><body contenteditable=\"true\">";
    private static final String CLOSING_HTML_TAG = "</body></html>";

    private static final double EDITOR_MIN_HEIGHT = 330;

    private static Jelog jelog;

    private String logbook;

    private ObservableList<Attachment> attachments = FXCollections.observableList(new ArrayList());

    private String author = null;

    private Button switchUserButton = new Button("Switch User");

    private TreeMap<String, List<String>> defaultAttributes = new TreeMap<>();

    private String defaultLogbook;

    private HashMap<String, String> triggered = new HashMap<>();

    private HashMap<String, List<Action>> actions = new HashMap<>();

    @FXML
    private Button buttonCancel;
    @FXML
    private Button buttonSubmit;
    @FXML
    private GridPane gridPaneLogbook;
    @FXML
    private GridPane gridPaneAttributes;
    @FXML
    private FlowPane thumbnailsPane;
    @FXML
    private Button addAttachmentButton;
    @FXML
    private VBox VBoxEditor;
    @FXML
    private TitledPane titledPaneAttachments;
    @FXML
    private HBox HBoxAttachmentsTitle;
    @FXML
    private ButtonBar buttonBar;
    @FXML
    private VBox vBoxParent;

    public void setDefaultLogbook(String logbook) {
        defaultLogbook = logbook;
    }

    /**
     * Default attributes are attributes that are set by applications using this
     * library.
     *
     * @param attributeName Name of the attribute.
     * @param options For checkboxes, separate arguments with commas ",".
     */
    public void addDefaultAttribute(String attributeName, List<String> options) {
        defaultAttributes.put(attributeName, options);
    }

    public void addDefaultAttribute(TreeMap<String, List<String>> attributes) {
        defaultAttributes.putAll(attributes);
    }

    private void setAuthor(String author) {
        this.author = author;
    }

    @Deprecated
    public void setSnapshots(WritableImage[] snapshots) {
        int i = 0;
        for (WritableImage snapshot : snapshots) {
            attachments.add(new Attachment("screenshot_" + Integer.toString(i) + ".png", snapshot));
            i++;
        }
    }

    public void addAttachments(List<Attachment> attachments) {
        this.attachments.addAll(attachments);
    }

    public void setAttachments(List<Attachment> attachments) {
        this.attachments.clear();
        addAttachments(attachments);
    }

    @Deprecated
    public void setAttachments(Attachment[] attachments) {
        this.attachments.clear();
        this.attachments.addAll(Arrays.asList(attachments));
    }

    private void setText(String text) {
        if (webView != null) {
            String escapedText = text.replaceAll("([^\\\\])(\")", "$1\\\\\"").replaceAll("\n", "\\\\n"); //Escaping quotation marks.
            // For a webview, we wait until the webview engine is ready.
            if (webView.getEngine().getLoadWorker().isRunning()) {
                ChangeListener<Boolean> cl = new ChangeListener<>() {
                    public void changed(ObservableValue<? extends Boolean> evW, Boolean oldValue, Boolean newValue) {
                        if (!newValue) {
                            webView.getEngine().executeScript("CKEDITOR.instances['Text'].setData(\"" + escapedText + "\")");
                            webView.getEngine().getLoadWorker().runningProperty().removeListener(this);
                        }
                    }
                };
                webView.getEngine().getLoadWorker().runningProperty().addListener(cl);
            } else {
                webView.getEngine().executeScript("CKEDITOR.instances['Text'].setData(\"" + escapedText + "\")");
            }
        } else {
            // Add opening and closing HTML tags.
            htmlEditor.setHtmlText(OPENING_HTML_TAG + text + CLOSING_HTML_TAG);
        }
    }

    /**
     * This method is used to set a logbook address different than the official.
     * It can only be called before the GUI is shown, otherwise it won't update
     * the properties.
     *
     * @param elogServer
     */
    public static void setElogServer(String elogServer) {
        PostEntryController.elogServer = elogServer.endsWith("/") ? elogServer : elogServer + '/';
    }

    /**
     * This method is used to set a Jelog object already initialized. It can
     * only be called before the GUI is shown, otherwise it won't update the
     * properties.
     *
     * @param jelog
     */
    public static void setJelog(Jelog jelog) {
        PostEntryController.jelog = jelog;
    }

    /**
     * Method to update the logbook server once the interface is running. Not
     * used at the moment.
     *
     * @param elogServer
     */
    public void updateElogServer(String elogServer) {
        PostEntryController.elogServer = elogServer.endsWith("/") ? elogServer : elogServer + '/';
        try {
            PostEntryController.jelog = new Jelog(elogServer);
            updateGridPaneLogbook();
        } catch (MalformedURLException ex) {
            Logger.getLogger(PostEntryController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        if (jelog == null) {
            try {
                jelog = new Jelog(elogServer);
            } catch (MalformedURLException ex) {
                Logger.getLogger(PostEntryController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        while (!jelog.isConnected()) {
            Dialog dialog = new Dialog();
            dialog.setTitle("Connection error");
            dialog.setHeaderText("Could not connect to the logbook server " + jelog.getServer() + ".");
            dialog.setContentText("Do you want to try again?");

            // Set the button types.
            ButtonType loginButtonType = new ButtonType("Ok", ButtonBar.ButtonData.OK_DONE);
            dialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);

            dialog.setResultConverter(dialogButton -> {
                return (dialogButton == loginButtonType);
            });

            Optional result = dialog.showAndWait();
            if (!result.isPresent() || (boolean) result.get() == false) {
                return;
            }
            // Try to reconnect
            jelog.retry();
        }

        switchUserButton.setStyle(" -fx-background-radius: 0px; -fx-padding: 2 5 2 5 ; -fx-border-color: #B0B0B0; -fx-border-width: 1 1 1 1; -fx-font-size: 9pt;");
        switchUserButton.setOnAction((ev) -> handleSwitchUserButton(ev));

        attachments.addListener(new ListChangeListener() {
            @Override
            public void onChanged(Change ch) {
                updateAttachments();
            }
        });

        HBoxAttachmentsTitle.minWidthProperty().bind(titledPaneAttachments.widthProperty());

        String editor = System.getProperty("eu.ess.jelog.ckeditor_path", "ckeditor.html");

        if (new File(editor).exists()) {
            webView = new WebView();
            webView.setMinHeight(EDITOR_MIN_HEIGHT);
            VBoxEditor.setPrefHeight(EDITOR_MIN_HEIGHT);
            webView.setPickOnBounds(true);
            VBoxEditor.getChildren().add(webView);
            try {
                webView.getEngine().load(new File(editor).toURI().toURL().toExternalForm());
            } catch (MalformedURLException ex) {
                Logger.getLogger(PostEntryController.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            htmlEditor = new HTMLEditor();
            htmlEditor.setMinHeight(EDITOR_MIN_HEIGHT);
            VBoxEditor.setPrefHeight(EDITOR_MIN_HEIGHT);
            VBoxEditor.getChildren().add(htmlEditor);
        }

        for (Region region : new Region[]{gridPaneLogbook, gridPaneAttributes, titledPaneAttachments, buttonBar}) {
            region.heightProperty().addListener((obs, wasExpanded, isNowExpanded) -> {
                setMinStageSize();
            });
        }

        // This need to be run after the scene and window are created and all elements shown.
        ChangeListener<Window> windowPropertyListener = new ChangeListener<>() {
            public void changed(ObservableValue<? extends Window> value, Window oldWindow, Window newWindow) {
                updateGridPaneLogbook();
                gridPaneAttributes.getScene().windowProperty().removeListener(this);
            }
        };

        ChangeListener<Scene> scenePropertyListener = new ChangeListener<>() {
            public void changed(ObservableValue<? extends Scene> value, Scene oldScene, Scene newScene) {
                newScene.windowProperty().addListener(windowPropertyListener);
                gridPaneAttributes.sceneProperty().removeListener(this);
            }
        };

        gridPaneAttributes.sceneProperty().addListener(scenePropertyListener);
    }

    public boolean login() {
        if (jelog.isConnected()) {
            // First tries to check if already logged in
            jelog.retrieveUsernameAndPassword();

            if (jelog.getUserFullName() == null) {
                AuthenticationPaneFX pane = new AuthenticationPaneFX();

                Optional<Pair<String, char[]>> result;
                do {
                    result = pane.showAndWait();
                    if (!result.isPresent()) {
                        return false;
                    }
                } while (jelog.login(result.get().getKey(), result.get().getValue(), true) == false);

                jelog.retrieveUsernameAndPassword();
            }

            setAuthor(jelog.getUserFullName());

            return true;
        } else {
            return false;
        }
    }

    /**
     * Updates the grid pane with all fields
     *
     * @param attributesMap
     */
    private void updateGridPaneLogbook() {
        if (defaultLogbook != null) {
            populateHardcodedLogbookRecursively(jelog.getConfiguration().getLogbookNode(defaultLogbook));
        } else {
            populateLogbooksRecursively(jelog.getConfiguration(), 0);
        }
    }

    private int populateHardcodedLogbookRecursively(LogbookNode node) {
        int level = -1;
        if (node == null) {
            Logger.getLogger(PostEntryController.class.getName()).info("Default logbook does not exists. Ignoring it...");
            populateLogbooksRecursively(jelog.getConfiguration(), 0);
        } else if (node.getParent() != null) {
            level = populateHardcodedLogbookRecursively(node.getParent());

            DecoratedLabel label;
            if (node.isGroup()) {
                label = new DecoratedLabel("Group");
            } else {
                label = new DecoratedLabel("Logbook");
                logbook = defaultLogbook;
                updateGridPaneAttributes();
            }

            ChoiceBoxControl control = new ChoiceBoxControl(new String[]{node.getName()});
            control.setDisable(true);

            gridPaneLogbook.add(label, 0, level);
            gridPaneLogbook.add(control, 1, level);
        }
        return level + 1;
    }

    private boolean populateLogbooksRecursively(LogbookNode node, int level) {
        TreeMap<String, LogbookNode> childNodes = (TreeMap) node.getNodes();
        if (node.isGroup()) {
            ChoiceBoxControl control = new ChoiceBoxControl(childNodes.keySet().toArray(new String[childNodes.keySet().size()]));

            ChoiceBox choiceBox = (ChoiceBox) control.getChild();
            choiceBox.getSelectionModel().selectFirst();

            boolean result = populateLogbooksRecursively(childNodes.get(childNodes.firstKey()), level + 1);
            DecoratedLabel label;
            if (result) {
                label = new DecoratedLabel("Logbook");
                choiceBox.valueProperty().addListener((a, oldValue, newValue) -> {
                    logbook = (String) newValue;
                    updateGridPaneAttributes();
                });
                logbook = childNodes.firstKey();
                updateGridPaneAttributes();
            } else {
                label = new DecoratedLabel("Group");
                choiceBox.valueProperty().addListener((a, oldValue, newValue) -> {
                    // Remove controls bellow this level
                    ArrayList<Node> childrenToRemove = new ArrayList<>();
                    for (Node child : gridPaneLogbook.getChildren()) {
                        if (GridPane.getRowIndex(child) > level) {
                            childrenToRemove.add(child);
                        }
                    }
                    gridPaneLogbook.getChildren().removeAll(childrenToRemove);
                    populateLogbooksRecursively(node.getLogbookNode((String) newValue), level + 1);
                });
            }

            gridPaneLogbook.add(label, 0, level);
            gridPaneLogbook.add(control, 1, level);

            return false;
        } else {
            return true;
        }
    }

    /**
     * Updates the grid pane with all fields. Called every time a different
     * logbook is selected.
     *
     * @param attributesMap
     */
    private void updateGridPaneAttributes() {
        // Cleans up the previous controls
        gridPaneAttributes.getChildren().clear();
        // Clear triggers and actions.
        triggered.clear();
        actions.clear();

        LinkedHashMap<String, LogbookAttribute> attributesMap = (LinkedHashMap) jelog.getLogbookAttributes(logbook);

        int i = 0;
        for (String attributeName : attributesMap.keySet()) {
            LogbookAttribute attribute = attributesMap.get(attributeName);

            if (!attributeName.equalsIgnoreCase(TEXT_ATTRIBUTE_NAME)) {

                DecoratedLabel label = new DecoratedLabel(attributeName);
                gridPaneAttributes.add(label, 0, i);

                Node node = null;
                if (attribute.getOptions() == null) {
                    node = new TextFieldControl();
                } else if (!attribute.isMultioption()) {
                    node = new ChoiceBoxControl(attribute.getOptions());
                } else if (attribute.isMultioption()) {
                    node = new CheckBoxControl(attribute.getOptions());
                }

                gridPaneAttributes.add(node, 1, i);

                Control control = (Control) node;

                if (attribute.isRequired()) {
                    label.setRequiredStyle();
                }

                if (attribute.isLocked()) {
                    control.setDisabled();
                }

                if (attributeName.equals("Author")) {
                    DecoratedHBox hBox = ((DecoratedHBox) control);
                    TextField authorTF = (TextField) hBox.getChild();
                    authorTF.setText(author);
                    hBox.setSpacing(10);
                    hBox.add(switchUserButton);
                }

                if (defaultAttributes.containsKey(attributeName)) {
                    control.setOptions(defaultAttributes.get(attributeName));
                }

                if (attribute.getTriggers() != null) {
                    for (String trigger : attribute.getTriggers()) {
                        if (trigger != "") {
                            control.registerTriggers(attributeName, attribute.getTriggers(), this);
                            break;
                        }
                    }
                }
            }

            for (Action action : attribute.getActions()) {
                if (!actions.containsKey(attributeName)) {
                    actions.put(attributeName, new ArrayList<>());
                }
                actions.get(attributeName).add(action);
            }

            i++;
        }

        // If Text is hardcoded, updated the editor.
        if (defaultAttributes.containsKey(TEXT_ATTRIBUTE_NAME) && !defaultAttributes.get(TEXT_ATTRIBUTE_NAME).isEmpty()) {
            setText(defaultAttributes.get(TEXT_ATTRIBUTE_NAME).get(0));
        }

        for (String attributeName : defaultAttributes.keySet()) {
            if (!attributesMap.containsKey(attributeName) && !attributeName.equals(TEXT_ATTRIBUTE_NAME)) {
                Logger.getLogger(PostEntryController.class.getName()).log(Level.SEVERE, "Attribute {0} not available in logbook {1}.", new String[]{attributeName, logbook});
            }
        }

        // Try to trigger actions in case they exist.
        triggerActions();
    }

    @FXML
    private void addAttachmentButtonAction(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Add attachment");
        File file = fileChooser.showOpenDialog(addAttachmentButton.getScene().getWindow());
        if (file != null) {
            try {
                attachments.add(new Attachment(file));

            } catch (IOException ex) {
                Logger.getLogger(PostEntryController.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void updateAttachments() {
        thumbnailsPane.getChildren().clear();
        if (!attachments.isEmpty()) {
            titledPaneAttachments.setExpanded(true);
            for (Attachment attachment : attachments) {
                AttachmentPane stackPane = new AttachmentPane(attachment, attachments);
                thumbnailsPane.getChildren().addAll(stackPane);
            }
        } else {
            titledPaneAttachments.setExpanded(false);
        }
    }

    private void handleSwitchUserButton(ActionEvent event) {
        jelog.logout();

        boolean loginResult = login();

        if (!loginResult) {
            handleButtonCancel();
        }
    }

    @FXML
    private void handleButtonSubmit(ActionEvent event) {
        TreeMap<String, List<String>> fields = new TreeMap();

        for (int i = 0; i < gridPaneAttributes.getChildren().size(); i += 2) {
            DecoratedLabel label = (DecoratedLabel) gridPaneAttributes.getChildren().get(i);

            List<String> options = null;

            if (gridPaneAttributes.getChildren().get(i + 1) instanceof TextFieldControl) {
                options = ((TextFieldControl) gridPaneAttributes.getChildren().get(i + 1)).getOptions();
            } else if (gridPaneAttributes.getChildren().get(i + 1) instanceof ChoiceBoxControl) {
                options = ((ChoiceBoxControl) gridPaneAttributes.getChildren().get(i + 1)).getOptions();
            } else if (gridPaneAttributes.getChildren().get(i + 1) instanceof CheckBoxControl) {
                options = ((CheckBoxControl) gridPaneAttributes.getChildren().get(i + 1)).getOptions();
            }

            fields.put(label.getAttribute(), options);

            if (label.isRequired() && (options == null || options.isEmpty())) {
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Error");
                alert.setHeaderText(null);
                alert.setContentText("Please fill all the required fields (red) before submitting the new entry.");
                alert.showAndWait();
                return;
            }
        }

        String text = null;
        if (webView != null) {
            text = (String) webView.getEngine().executeScript("CKEDITOR.instances['Text'].getData()");
        } else {
            text = htmlEditor.getHtmlText();
            // Remove unnecessary HTML tags.
            int startIdx = OPENING_HTML_TAG.length();
            int endIdx = text.length() - CLOSING_HTML_TAG.length();
            text = text.substring(startIdx, endIdx);
        }

        try {
            jelog.submit(fields, text, "HTML", attachments, logbook);

        } catch (IOException ex) {
            Logger.getLogger(PostEntryController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        Stage stage = (Stage) buttonSubmit.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void handleButtonCancel() {
        Stage stage = (Stage) buttonCancel.getScene().getWindow();
        stage.close();
    }

    private void setMinStageSize() {
        double size = 0;

        List<Region> elements = new ArrayList<>();
        elements.add(gridPaneLogbook);
        elements.add(gridPaneAttributes);
        elements.add(titledPaneAttachments);
        elements.add(VBoxEditor);
        elements.add(buttonBar);

        for (Region element : elements) {
            size += element.getHeight();
//            size += VBox.getMargin(element).getTop();
//            size += VBox.getMargin(element).getBottom();
        }

        // Take into account the minsize of the editor.
        if (webView != null && webView.getHeight() > EDITOR_MIN_HEIGHT) {
            size += EDITOR_MIN_HEIGHT - webView.getHeight();
        } else if (htmlEditor != null && htmlEditor.getHeight() > EDITOR_MIN_HEIGHT) {
            size += EDITOR_MIN_HEIGHT - htmlEditor.getHeight();
        }
        size += buttonBar.getHeight();

        vBoxParent.setMinHeight(size);
    }

    @Override
    public void trigger(String attributeName, String trigger) {
        triggered.put(attributeName, trigger);
        triggerActions();
    }

    private void triggerActions() {
        for (String attributeName : actions.keySet()) {
            for (Action action : actions.get(attributeName)) {
                if (action.evaluate(triggered.values())) {
                    // Find the control for that attribute.
                    if (attributeName.equalsIgnoreCase(TEXT_ATTRIBUTE_NAME)) {
                        String text = action.getAction().get(0);
                        if (text.endsWith(".txt") || text.endsWith(".htm") || text.endsWith(".html")) {
                            String fileName = text.replace("templates", TEMPLATES_URL);

                            try {
                                InputStream file = new URL(jelog.getServerURL(), fileName).openStream();
                                BufferedReader reader = new BufferedReader(new InputStreamReader(file));
                                String line;
                                text = "";
                                while ((line = reader.readLine()) != null) {
                                    text = text.concat(line + "\n");
                                }
                            } catch (MalformedURLException ex) {
                                Logger.getLogger(PostEntryController.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (IOException ex) {
                                Logger.getLogger(PostEntryController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            setText(text);
                        }
                    } else {
                        for (int i = 0; i < gridPaneAttributes.getChildren().size(); i += 2) {
                            DecoratedLabel label = (DecoratedLabel) gridPaneAttributes.getChildren().get(i);
                            if (label.getAttribute().equals(attributeName)) {
                                ((Control) gridPaneAttributes.getChildren().get(i + 1)).setOptions(action.getAction());
                            }
                        }
                    }
                }
            }
        }
    }
}
