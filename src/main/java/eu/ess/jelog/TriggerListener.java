/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.ess.jelog;

/**
 *
 * @author juanfestebanmuller
 */
public interface TriggerListener {

    public void trigger(String attributeName, String trigger);

}
