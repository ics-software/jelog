/*
 * Copyright (C) 2018 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package eu.ess.jelog;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 */
public class LogbookAttribute {

    private boolean required;
    private boolean locked;
    private boolean radiooption;
    private boolean multioption;
    private boolean preset;
    private String[] options;
    private String[] triggers; // Triggers for automatic actions.
    private List<Action> actions = new ArrayList<>(); // Actions to executes

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public boolean isRequired() {
        return required;
    }

    public void setMultioption(boolean multioption) {
        this.multioption = multioption;
    }

    public boolean isMultioption() {
        return multioption;
    }

    public void setRadiooption(boolean radiooption) {
        this.radiooption = radiooption;
    }

    public boolean isRadiooption() {
        return radiooption;
    }

    public void setPreset(boolean preset) {
        this.preset = preset;
    }

    public boolean isPreset() {
        return preset;
    }

    public void setOptions(String[] options) {
        this.options = options;
    }

    public String[] getOptions() {
        return options;
    }

    public String[] getTriggers() {
        return triggers;
    }

    public void setTriggers(String[] triggers) {
        this.triggers = triggers;
    }

    public List<Action> getActions() {
        return actions;
    }

    public void addActions(Action action) {
        actions.add(action);
    }

    public LogbookAttribute(boolean required, boolean locked, boolean multioption, boolean radiooption,
            String[] options, String[] triggers) {
        this.locked = locked;
        this.required = required;
        this.multioption = multioption;
        this.radiooption = radiooption;
        this.options = options;
        this.triggers = triggers;

    }

    public LogbookAttribute(LogbookAttribute attribute) {
        this(attribute.locked, attribute.locked, attribute.multioption, attribute.radiooption,
                null, null);
        if (attribute.options != null) {
            this.options = attribute.options.clone();
        }
        if (attribute.triggers != null) {
            this.triggers = attribute.triggers.clone();
        }
    }

    public LogbookAttribute() {
        this(false, false, false, false, null, null);
    }
}
