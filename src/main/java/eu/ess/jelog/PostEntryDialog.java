/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package eu.ess.jelog;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.scene.layout.Region;

/**
 * Class to submit new entries using a JavaFX dialog. Useful for logging data
 * together with some files or images attached directly from a JavaFX or Open
 * XAL application.
 *
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 */
public class PostEntryDialog {

    private PostEntryDialog() {
    }

    public static void setElogServer(String elogserver) {
        PostEntryController.setElogServer(elogserver);
    }

    public static int post(Map<String, List<String>> defaultAttributes) throws IOException {
        return post(null, null, defaultAttributes);
    }

    public static int post(String defaultLogbook, Map<String, List<String>> defaultAttributes) throws IOException {
        return post(null, defaultLogbook, defaultAttributes);
    }

    public static int post(List<Attachment> attachments, Map<String, List<String>> defaultAttributes) throws IOException {
        return post(attachments, null, defaultAttributes);
    }

    public static int post(List<Attachment> attachments, String defaultLogbook, Map<String, List<String>> defaultAttributes) throws IOException {
        Stage stage = null;

        FXMLLoader fxmlLoader = new FXMLLoader(PostEntryController.class.getResource("/fxml/PostEntryScene.fxml"));

        Parent root = (Parent) fxmlLoader.load();

        PostEntryController controller = fxmlLoader.<PostEntryController>getController();

        if (defaultLogbook != null && !defaultLogbook.isBlank()) {
            controller.setDefaultLogbook(defaultLogbook);
        }

        if (defaultAttributes != null && !defaultAttributes.isEmpty()) {
            for (Entry<String, List<String>> attribute : defaultAttributes.entrySet()) {
                controller.addDefaultAttribute(attribute.getKey(), attribute.getValue());
            }
        }

        if (controller.login()) {
            if (attachments != null && !attachments.isEmpty()) {
                controller.setAttachments(attachments);
            }

            Scene scene = new Scene(root);
            scene.getStylesheets().add(PostEntryController.class.getResource("/styles/jelog.css").toExternalForm());

            stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setTitle("elog");
            stage.setScene(scene);

            stage.minHeightProperty().bind(((Region) root).minHeightProperty().add(stage.getHeight() - ((Region) root).getHeight()));

            stage.showAndWait();
        }

        return 0;
    }
}
